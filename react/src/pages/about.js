import React from "react"
import Markdown from "react-markdown"
import aboutText from "../pages.json"
import Layout from "../components/layout"
import ColorPicker2Component from "../components/colorpicker2"

const About = () => {
    return (
        <Layout>
             <h1 style={{textAlign: `center`, marginBottom: `40px`}}>This is the About Page.</h1>
            <div className="page-content">
                <Markdown source={aboutText[0].content} escapeHtml={false} />
            </div>
            <ColorPicker2Component />
        </Layout>
    )
}

export default About