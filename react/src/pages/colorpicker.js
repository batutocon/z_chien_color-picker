import React from "react"

import Layout from "../components/layout"
import "./pages.css"
import ImageColorPicker from "../components/colorpicker-img"

const ColorPicker = () => {
    return (
        <div>
            <Layout>
                <h1>ColorPicker</h1>
                <ImageColorPicker/>
            </Layout>
        </div>
    )
}

export default ColorPicker