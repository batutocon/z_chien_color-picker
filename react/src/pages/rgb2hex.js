import React from "react"

import Layout from "../components/layout"
import ColorPickerComponent from "../components/colorpicker"
import "./pages.css"

const rgb2hex = () => {
    return (
        <div>
            <Layout>
                <h1>RGB to HEX</h1>
                <div>
                    <ColorPickerComponent/>
                </div>
            </Layout>
        </div>
    )
}

export default rgb2hex