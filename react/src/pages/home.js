import React from "react"

import Layout from "../components/layout"
import ColorPickerComponent from "../components/colorpicker"
import "./pages.css"

const Home = () => {
    return (
        <div>
            <Layout>
                <h1>Home</h1>
                <div>
                    <ColorPickerComponent/>
                </div>
            </Layout>
        </div>
    )
}

export default Home