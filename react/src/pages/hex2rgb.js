import React from "react"

import Layout from "../components/layout"
import ColorPickerComponent from "../components/colorpicker"
import "./pages.css"

const hex2rgb = () => {
    return (
        <div>
            <Layout>
                <h1>Hex to RGB</h1>
                <div>
                    <ColorPickerComponent/>
                </div>
            </Layout>
        </div>
    )
}

export default hex2rgb