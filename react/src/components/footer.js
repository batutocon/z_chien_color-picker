import React from "react"
import { Link } from "react-router-dom"

const Footer = () => {
    return (
        <div className="footer">
           <div className="navbar header">
                <Link className="links-2" to="/">About Us</Link>
                <Link className="links-2" to="/about">Contact Us</Link>
                <Link className="links-2" to="/about">Privacy Notice</Link>
            </div>
        </div>
    )
}

export default Footer