import React from "react";
import "./components.css";
import logo from "../logo.png"; // Import the image

const Header = () => {
    const headerStyle = {
        textAlign: "center"
    };
    const headerIMGStyle = {
        height: "50px"
    };
    return (
        <div className="header" style={headerStyle}>
            <img src={logo} style={headerIMGStyle} alt="Logo" />
        </div>
    );
}

export default Header;