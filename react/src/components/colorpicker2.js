import React from 'react';
import { PhotoshopPicker } from 'react-color';

class ColorPicker2Component extends React.Component {
  state = {
    background: '#fff',
  };

  handleChangeComplete = (color, event) => {
    this.setState({ background: color.hex });
  };

  render() {
    return <PhotoshopPicker onChangeComplete={ this.handleChangeComplete } />;
  }
}

export default ColorPicker2Component