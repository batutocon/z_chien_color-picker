import React, { useState, useRef } from 'react';

const ImageColorPicker = () => {
  const [color, setColor] = useState({ hex: '#ffffff', rgb: 'rgb(255, 255, 255)' });
  const [imageSrc, setImageSrc] = useState(null);

  const inputRef = useRef();

  const handleImageClick = (event) => {
    if (inputRef.current) {
      const canvas = document.createElement('canvas');
      const context = canvas.getContext('2d');

      canvas.width = inputRef.current.width;
      canvas.height = inputRef.current.height;

      context.drawImage(inputRef.current, 0, 0, canvas.width, canvas.height);

      const pixel = context.getImageData(event.nativeEvent.offsetX, event.nativeEvent.offsetY, 1, 1).data;

      const hexColor = rgbToHex(pixel[0], pixel[1], pixel[2]);
      const rgbColor = `rgb(${pixel[0]}, ${pixel[1]}, ${pixel[2]})`;

      setColor({ hex: hexColor, rgb: rgbColor });
    }
  };

  const handleFileInputChange = (event) => {
    const file = event.target.files[0];

    if (file) {
      const reader = new FileReader();

      reader.onload = (e) => {
        const imageSrc = e.target.result;
        setImageSrc(imageSrc);
      };

      reader.readAsDataURL(file);
    }
  };

  const handleLabelClick = () => {
    inputRef.current.click();
  };

  const rgbToHex = (r, g, b) => {
    return `#${(1 << 24 | r << 16 | g << 8 | b).toString(16).slice(1)}`;
  };

  return (
    <div>
      <input
        type="file"
        accept="image/*"
        onChange={handleFileInputChange}
        style={{ display: 'none' }}
        ref={inputRef}
      />
      <label htmlFor="fileInput" onClick={handleLabelClick}>
        Choose Image
      </label>
      {imageSrc && (
        <div>
          <img
            ref={inputRef}
            src={imageSrc}
            alt="Selected Image"
            style={{ maxWidth: '100%', maxHeight: '40vh' }}
            onClick={handleImageClick}
          />
          <div>
            <h2>Selected Color:</h2>
            <p>Hex: {color.hex}</p>
            <p>RGB: {color.rgb}</p>
          </div>
          <div
            style={{
              width: '100px',
              height: '100px',
              backgroundColor: color.hex,
              margin: '10px',
              border: '1px solid #000',
            }}
          ></div>
        </div>
      )}
    </div>
  );
};

export default ImageColorPicker;
