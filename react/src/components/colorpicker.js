import React, { useState } from 'react';
import { SketchPicker } from 'react-color';
import 'react-image-picker/dist/index.css';

const ColorPickerComponent = () => {
  const [color, setColor] = useState('#ffffff'); // Initial color
  const [selectedPixelColor] = useState(null);
  let rgb = '';
  const handleColorChange = (newColor) => {
    setColor(newColor.hex);
    debugger;
    rgb = `rgb(${newColor.rgb.r}, ${newColor.rgb.g}, ${newColor.rgb.b})`;
  };


  return (
    <div>
      <div>
          <h2>Selected Color: {color}</h2>
          <h2>Selected Color: {rgb}</h2>
          <SketchPicker color={color} onChange={handleColorChange} />
          {selectedPixelColor && (
            <div>
              <h3>Color at clicked pixel:</h3>
              <p>Hex: {selectedPixelColor.hex}</p>
              <p>RGB: {selectedPixelColor.rgb}</p>
            </div>
          )}
        </div>
    </div>
  );
};

export default ColorPickerComponent;
