import React from "react"
import { Link } from "react-router-dom"

const Navbar = () => {
    return (
        <div className="navbar header">
            <Link className="links-2" to="/">Home</Link>
            <Link className="links-2" to="/hex2rgb">HEX to RGB</Link>
            <Link className="links-2" to="/rgb2hex">RGB to HEX</Link>
            <Link className="links-2" to="/colorpicker">Image Color Picker</Link>
            <Link className="links-2" to="/blog">Blog</Link>
        </div>
    )
}

export default Navbar