import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Route } from "react-router-dom"
import './index.css';
import "typeface-raleway"
import Home from "./pages/home"
import About from "./pages/about"
import Post from "./pages/post"
import ColorPicker from "./pages/colorpicker"
import Blog from "./pages/blog"
import rgb2hex from "./pages/rgb2hex"
import hex2rgb from "./pages/hex2rgb"

ReactDOM.render(
    <Router>
        <div>
            <Route exact path="/" component={Home} />
            <Route exact path="/blog" component={Blog} />
            <Route exact path="/about" component={About} />
            <Route exact path="/colorpicker" component={ColorPicker} />
            <Route exact path="/rgb2hex" component={rgb2hex} />
            <Route exact path="/hex2rgb" component={hex2rgb} />
            <Route exact path="/post/:id" render={props => <Post {...props} />} />
        </div>
    </Router>,
    document.getElementById('root')
);

